# **MAPAS CONCEPTUALES**
## *INDUSTRIA 4.0*

```plantuml
@startmindmap
caption 18161151: Lazaro Lopez Juan Luis
title Industria 4.0

*[#0CACED] **Industria 4.0***
 *_ Es
  *[#lightgreen] Integración de tecnologías \nde procesamiento \nde datos software \ninteligente y sensores 
   *_ Lo cual modifica
    *[#lightgreen] Nuevas culturas digitales
     *_ Donde
      *[#lightgreen] Era de los phono Sapiens
       *_ Son
        *[#lightgreen] Son personas acostumbradas \na usar dispositivos móviles      
    *[#lightgreen] Aparición de nuevos \nparadigmas y tecnologías
     *_ Donde
      *[#lightgreen] Surgen nuevos paradigmas
       *_ Como
        *[#lightgreen] Velocidad (Adaptacion)
        *[#lightgreen] Negocios basados en \nplataformas (Fang y Bat)
        *[#lightgreen] Internet de \nlas cosas
        *[#lightgreen] Big data e \ninteligencia artificial
    *[#lightgreen] Transformación de las \nempresas de manufactura \nen empresas de tics
     *_ Efectos
      *[#lightgreen] Cada vez existen menos \ndiferenciación entre las \nindustrias
     *_ Donde
      *[#lightgreen] Implementación de soluciones \nde tecnologías de información      
    *[#lightgreen] Integración de las TIC's \nen la industria de \nmanufactura y de servicios
     *_ Efectos
      *[#lightgreen] Reducción de los \npuestos de trabajo
      *[#lightgreen] Aparición de \nnuevas profesiones
     *_ Donde
      *[#lightgreen] El proceso es \nautomatizado
       *_ Gracias a
        *[#lightgreen] Sensores y LoT
        *[#lightgreen] Redes de alta Velocidad
        *[#lightgreen] Machine Learning y Big Data
        *[#lightgreen] Robots Autónomos
      *[#lightgreen] Hay cada vez menos \nparticipación de personas
 *_ Se busca
  *[#57D992] Producción inteligente con \ndecisión autónoma
   *_ Gracias a
    *[#57D992] El desarrollo de \nnuevas tecnologías
     *_ Como
      *[#57D992] Inteligencia \nartificial
      *[#57D992] Big Data
      *[#57D992] Cloud Computing
      *[#57D992] Robotica
      *[#57D992] Internet de \nlas cosas
 *_ Sus antecesores fueron
  *[#6F1CDF] Industria 1.0
   *_ Conocida como
    *[#6F1CDF] Primera Revolucion \nIndustrial
     *_ La cual
      *[#6F1CDF] Inicio en 1784
       *_ Caracterizada por
        *[#6F1CDF] La produccion basada \nen maquinaria \ny aparición de transportes \nde carga masivos
        *[#6F1CDF] La invención de \nla energía de \nvapor y a \nla hidráulica
        *[#6F1CDF] Se incrementa \nla producción \ny disminuyó \nel costo
  *[#2E5DDD] Industria 2.0
   *_ La cual fue
    *[#2E5DDD] La segunda \nRevolucion Industrial
     *_ En
      *[#2E5DDD] 1870
       *_ Caracterizada por
        *[#2E5DDD] La invención de la \nenergía eléctrica y \nla línea de ensamblaje
        *[#2E5DDD] La invención de \nla energía de \nvapor y a \nla hidráulica
        *[#2E5DDD] Se incrementa \nla producción \ny disminuyó \nel costo
  *[#6868EE] Industria 3.0
   *_ En
    *[#6868EE] 1969
     *_ Caracterizada por
      *[#6868EE] Producción automatizada \nde productos 
      *[#6868EE] La invención de \nla energía de \nvapor y a \nla hidráulica
      *[#6868EE] Aplicación de electrónica \ny de la tecnología de \nla información
    
@endmindmap
```
 
# *MÉXICO Y LA INDUSTRIA 4.0*
```plantuml
@startmindmap
caption 18161151: Lazaro Lopez Juan Luis
title México y Industria 4.0

*[#57D3D9] **México y \la Industria 4.0***
 *_ El Desarrollo
  *[#AA74FE] De México es muy cercano \nen la industria 4.0
   *_ Ya que 
    *[#AA74FE] Ha empezado con la implementación \nde la digitalización
     *_ Como ejemplo
      *[#AA74FE] StartUP
       *_ Con
        *[#AA74FE] Sin llave
         *_ Gracias a
          *[#AA74FE] Internet de \nlas cosas
        *[#AA74FE] WearROBOT
    *[#AA74FE] Cuenta con una \nrelacion industrial \ncon grandes empresas
   *_ Sin embargo 
    *[#AA74FE] Es necesario que \ndisponga de infraestructuras
     *_ Que permitan 
      *[#AA74FE] A empresas y personas \ntener acceso a internet \ny nuevas tecnologías
       *_ A fin de 
        *[#AA74FE] Optimizar sus capacidades productivas
 *_ Para esto
  *[#A6FA70] Los aspectos a tomar \nen cuenta ën la implementacion \nla industria 4.0
   *_ Son
    *[#A6FA70] Visibilidad
     *_ Lo cual es
      *[#A6FA70] La recolección de \ndatos del campo
    *[#A6FA70] Adaptación
     *_ Lo cual es
      *[#A6FA70] La viabilidad
    *[#A6FA70] Transparencia
     *_ Lo cual es
      *[#A6FA70] Conocimiento de \nlos suceso en el \nentorno
 *_ Tiene beneficios como
  *[#DDBFF6] Optimización
   *_ Consiste
    *[#DDBFF6] Aumentar la productividad y producción
  *[#DDBFF6] Reducción de costos
   *_ Es decir
    *[#DDBFF6] Disminuir costos
  *[#DDBFF6] Monitorización \nen tiempo real
   *_ Para asi
    *[#DDBFF6] Obtener la información \ndel entorno del momento 
  *[#DDBFF6] Obtencion de datos
   *_ Consiste
    *[#DDBFF6] Obtencion de datos \nsobre la produccion \ny procesos
 *_ Factores en contra
  *[#F76372] Desinformación
   *_ Ya que
    *[#F76372] Algunas empresas \npequeñas/medianas no \ntienen conocimiento sobre \nesto
  *[#F76372] Inversiones
   *_ Es decir
    *[#F76372] No se tiene idea \nde la inversion y \nlos resultados

@endmindmap
```
:smile:

### *Recursos:*
[Industria 4 0 - Explicado Fácilmente](https://youtu.be/Qb7twp03c58)  
[México: hacia la Industria 4.0](https://youtu.be/QfWjtWf8-6A)  
[Conferencia Magistral "México en la Industria 4.0"](https://youtu.be/riMGgWLpQdM)  